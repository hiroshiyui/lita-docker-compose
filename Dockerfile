FROM ruby:2.5.3

RUN mkdir /lita
WORKDIR /lita
COPY . /lita
RUN bundle install

CMD "lita"